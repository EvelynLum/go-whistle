package handler

import (
	"encoding/json"
	"go-whistle-api/statistics/v2/domain"
	"net/http"
)

type CommonResponse struct {
	Data  interface{} `json:"data"`
	Error interface{} `json:"err"`
}

func Ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	resp := &CommonResponse{
		Data: domain.Ping(),
	}
	jResp, _ := json.Marshal(resp)
	w.Write([]byte(jResp))
}
