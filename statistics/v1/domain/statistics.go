package domain

import (
	dbStore "go-whistle-api/statistics/v1/store"
	"strings"
	"time"
)

func Ping() string {
	return "Pong statistics v1"
}

func AddArticleViewCount(articleId int64) (Article, error) {
	if _, err := increaseArticleTotalViewCount(articleId); err != nil {
		return Article{}, err
	}

	_, err := addArticleViewHistory(articleId)
	if err != nil {
		return Article{}, err
	}

	return Article{
		Id: articleId,
	}, nil
}

func increaseArticleTotalViewCount(articleId int64) (int64, error) {
	artStore := dbStore.NewArticle()
	rowAffected, err := artStore.IncrementTotalViewCountByArticleID(articleId)
	if err != nil {
		return 0, err
	}
	return rowAffected, nil
}

func addArticleViewHistory(articleId int64) (int64, error) {
	artHistStore := dbStore.NewArticleHistory()
	history := dbStore.ArticleViewHistory{
		ArticleId: articleId,
	}
	id, err := artHistStore.Add(history)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func GetArticleViewStatistics(articleId int64) (ArticleViewStat, error) {
	mainArt, err := getArticleStat(articleId)
	if err != nil {
		return ArticleViewStat{}, err
	}

	artHistories, err := getPastThreeDaysReads(articleId)
	if err != nil {
		return ArticleViewStat{}, err
	}
	detailedStats, err := computeArticleHistoryStat(artHistories)
	if err != nil {
		return ArticleViewStat{}, err
	}

	stat := ArticleViewStat{
		ArticleId:      articleId,
		Type:           "statistic_article_view_count",
		TotalViewCount: mainArt.TotalViewCount,
		Attibutes:      AttributeList{Count: detailedStats},
	}
	return stat, nil
}

func getArticleStat(articleId int64) (Article, error) {
	artStore := dbStore.NewArticle()
	article, err := artStore.GetByArticleID(articleId)
	if err != nil {
		return Article{}, err
	}

	return Article{
		Id:             article.Id,
		Name:           article.Name,
		TotalViewCount: article.TotalViewCount,
	}, nil
}

func getPastThreeDaysReads(articleId int64) ([]ArticleHistory, error) {
	artHistStore := dbStore.NewArticleHistory()
	histories, err := artHistStore.GetPastThreeDay(articleId)
	if err != nil {
		return nil, err
	}

	var hist []ArticleHistory
	for _, h := range histories {
		hist = append(hist, ArticleHistory{
			ReadTs: h.ReadTs,
		})
	}
	return hist, nil
}

func getDurationTemplate() []*DurationNode {
	var durationTemplate []*DurationNode
	durationTemplate = append(durationTemplate, &DurationNode{MinDiff: 5, MinDesc: "5 minutes ago"})
	durationTemplate = append(durationTemplate, &DurationNode{MinDiff: 60, MinDesc: "1 hour ago"})
	durationTemplate = append(durationTemplate, &DurationNode{MinDiff: 1440, MinDesc: "1 day ago"})
	durationTemplate = append(durationTemplate, &DurationNode{MinDiff: 2880, MinDesc: "2 days ago"})
	durationTemplate = append(durationTemplate, &DurationNode{MinDiff: 4320, MinDesc: "3 days ago"})

	return durationTemplate
}

func computeArticleHistoryStat(histories []ArticleHistory) ([]*DurationNode, error) {
	attrs := getDurationTemplate()

	now := time.Now()

	for _, hist := range histories {
		histTs, err := formatDtToRFC3339(hist.ReadTs)
		if err != nil {
			return nil, err
		}

		recMinDiff := now.Sub(histTs)
		for _, attr := range attrs {
			if int(recMinDiff.Minutes()) <= attr.MinDiff {
				attr.Count = attr.Count + 1
			}
		}
	}

	return attrs, nil
}

func formatDtToRFC3339(dtFromDb string) (time.Time, error) {
	dtFromDb = strings.Replace(dtFromDb, " ", "T", 1)
	dtFromDb = string(dtFromDb[0:19]) + "Z"

	dtInTimeCxt, err := time.Parse(time.RFC3339, dtFromDb)
	if err != nil {
		return time.Now(), err
	}
	return dtInTimeCxt, nil
}
