package domain

type Article struct {
	Id             int64
	Name           string
	TotalViewCount int64
}

type ArticleViewStat struct {
	ArticleId      int64
	Type           string
	TotalViewCount int64
	Attibutes      AttributeList
}

type AttributeList struct {
	Count []*DurationNode
}

type DurationNode struct {
	MinDiff int
	MinDesc string
	Count   int16
}

type ArticleHistory struct {
	Id        int64
	ArticleId int64
	ReadTs    string
}
