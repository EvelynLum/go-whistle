package handler

import (
	"encoding/json"
	"fmt"
	"go-whistle-api/statistics/v1/domain"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type CommonResponse struct {
	Data  interface{} `json:"data"`
	Error interface{} `json:"err"`
}

func Ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	resp := &CommonResponse{
		Data: domain.Ping(),
	}
	jResp, _ := json.Marshal(resp)
	w.Write([]byte(jResp))
}

func TrackArticle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	resp := &CommonResponse{}

	reqBody := DataReq{}
	err := json.NewDecoder(r.Body).Decode(&reqBody)
	if err != nil {
		resp.Error = err
	}
	// TODO: refactor better if else management
	id, err := strconv.Atoi(reqBody.Data.ArticleId)

	if err != nil {
		resp.Error = err
	} else {
		art, err := domain.AddArticleViewCount(int64(id))
		if err != nil {
			resp.Error = err
		} else {
			resp.Data = convertTrackArticleDomainToAPIModel(art)
		}
	}

	jResp, err := json.Marshal(resp)
	if err != nil {
		fmt.Println(err)
	}
	w.Write([]byte(jResp))
}

func convertTrackArticleDomainToAPIModel(dArt domain.Article) Article {
	return Article{
		Id: dArt.Id,
	}
}

func GetArticleViewCount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	resp := &CommonResponse{}

	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		resp.Error = err
	} else {
		art, err := domain.GetArticleViewStatistics(int64(id))
		if err != nil {
			resp.Error = err
		} else {
			resp.Data = convertGetArticleDomainToAPIModel(art)
		}
	}

	jResp, err := json.Marshal(resp)
	if err != nil {
		fmt.Println(err)
	}
	w.Write([]byte(jResp))
}

func convertGetArticleDomainToAPIModel(dArt domain.ArticleViewStat) ViewCount {
	var dAttrs []*domain.DurationNode = dArt.Attibutes.Count
	var apiAttrs []CountInDuration
	for _, dAttr := range dAttrs {
		apiAttrs = append(apiAttrs, CountInDuration{InPast: dAttr.MinDesc, Count: dAttr.Count})
	}
	apiAttrsCounts := Counts{Count: apiAttrs}

	return ViewCount{
		ArticleId:      string(dArt.ArticleId),
		Type:           dArt.Type,
		TotalViewCount: dArt.TotalViewCount,
		Attibutes:      apiAttrsCounts,
	}
}
