package handler

type Article struct {
	Id int64 `json:"article_id"`
}

type ViewCount struct {
	ArticleId      string `json:"article_id"`
	Type           string `json:"type"`
	TotalViewCount int64  `json:"total_view_count"`
	Attibutes      Counts `json:"attributes"`
}

type Counts struct {
	Count []CountInDuration `json:"count"`
}

type CountInDuration struct {
	InPast string `json:"reference"`
	Count  int16  `json:"count"`
}
