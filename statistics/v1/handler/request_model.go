package handler

type DataDetailReq struct {
	ArticleId string `json:"article_id"`
}

type DataReq struct {
	Data DataDetailReq `json:"data"`
}
