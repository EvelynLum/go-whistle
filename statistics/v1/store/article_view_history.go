package store

import (
	"go-whistle-api/middleware"

	_ "github.com/lib/pq"
)

type ArticleViewHistory struct {
	Id        int64
	ArticleId int64
	ReadTs    string
}

func NewArticleHistory() *ArticleViewHistory {
	return &ArticleViewHistory{}
}

func (vhDb *ArticleViewHistory) GetPastThreeDay(articleID int64) ([]ArticleViewHistory, error) {
	db, err := middleware.CreateConnection()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	sql := `SELECT id, article_id, read_ts
			FROM statistics.article_view_history
			WHERE article_id=$1
			AND read_ts >= (NOW() - interval '3 days')
			AND read_ts <= NOW()
			ORDER BY read_ts;`

	rows, err := db.Query(sql, articleID)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var histories []ArticleViewHistory
	for rows.Next() {
		var history ArticleViewHistory
		err = rows.Scan(&history.Id, &history.ArticleId, &history.ReadTs)
		if err != nil {
			return nil, err
		}

		histories = append(histories, history)
	}

	return histories, err
}

func (vhDb *ArticleViewHistory) Add(h ArticleViewHistory) (int64, error) {
	db, err := middleware.CreateConnection()
	if err != nil {
		return 0, err
	}
	defer db.Close()

	sql := `INSERT INTO statistics.article_view_history
			(article_id, read_ts)
			VALUES($1, NOW())
			RETURNING id;`

	var id int64
	err = db.QueryRow(sql, h.ArticleId).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}
