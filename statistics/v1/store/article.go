package store

import (
	"go-whistle-api/middleware"

	_ "github.com/lib/pq"
)

type Article struct {
	Id             int64
	Name           string
	TotalViewCount int64
}

func NewArticle() *Article {
	return &Article{}
}

func (aDb *Article) GetByArticleID(articleID int64) (Article, error) {
	db, err := middleware.CreateConnection()
	if err != nil {
		return Article{}, err
	}
	defer db.Close()

	sql := `SELECT *
			FROM statistics.article
			WHERE id=$1;`

	row := db.QueryRow(sql, articleID)
	var article Article
	err = row.Scan(&article.Id, &article.Name, &article.TotalViewCount)
	if err != nil {
		return Article{}, err
	}
	return article, nil
}

func (aDb *Article) IncrementTotalViewCountByArticleID(articleID int64) (int64, error) {
	db, err := middleware.CreateConnection()
	if err != nil {
		return 0, err
	}
	defer db.Close()

	sql := `UPDATE statistics.article
			SET total_view_count = total_view_count + 1
			WHERE id = $1;`

	res, err := db.Exec(sql, articleID)
	if err != nil {
		return 0, err
	}
	rowAffected, _ := res.RowsAffected()
	return rowAffected, nil
}
