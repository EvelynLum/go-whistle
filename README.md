# go-whistle

## Prerequisties
1. Install golang according to the [offical instruction](https://golang.org/doc/install).
1.
1. Install PostgreSQL (include pgAdmin).
1. Install Postman.

### Setup go-whistle environment
#### Setup backend code
1. Navigate to $GOPATH/src folder.
1. Clone the go-whistle repository.
    `git clone https://gitlab.com/EvelynLum/go-whistle.git`

#### Setup database
1. Open pgAdmin (Guides below will use pgAdmin) or other DB client that supports PostgreSQL.
1. After connected to server, expand the Server - Databases.
1. Click on the 'postgres' database.
1. Right-click, an action list will be displayed.
1. Click on the 'Query Tool...'.
1. Create 'go-whistle' databse.
    1. Go to the cloned folder above.
    1. Navigate to 'setup' foler.
    1. Copy the content of 'setup-db.sql'.
    1. Paste in the 'Query Tool' in the pgAdmin above.
    1. Execute the SQL to install 'go-whistle' database.
1. (Re)Create tables.
    1. Go back to the cloned 'go-whistle/setup' folder.
    1. Copy the content of the 'go-whistle-db-setup.sql'.
    1. Paste it in the 'Query Tool' in pgAdmin.
    1. Click on the 'go-whistle' database icon to indicate it as the active database.
    1. Execute the SQL to drop and create tables.
1. *You may repeat the '(Re)Create tables' steps without create database when necessary (Apply new table; start with fresh tables).*
1. Update connection string in golang application.
    1. Go to '$GOPATH/src/go-whistle' folder.
    1. Edit '.env' file with your favourite editor.
    1. Update configurations whichever apply from the setup above.
    1. *You may check the connection info from the pgAdmin:*
        1. Go to pgAdmin.
        1. Right-click server (One level above 'Databases').
        1. Click 'Properties'.
        1. Click on the 'Connection' tab.
        1. Adjust the query string based on the information displayed here.

#### Setup Postman
1. Open Postman.
1. Import 'go-whistle' collection.
    1. Click 'Import' button at the top left corner.
    1. In the 'Import File' tab, click 'Choose files' button.
    1. Navigate to cloned 'go-whistle/setup' folder.
    1. Select 'go-whistle.postman_collection.json' file to import.
    1. You will see 'go-whitle' collection shows in the 'Collections' panel.
1. Create 'go-whistle' environment.
    1. Click the 'Manage Environments' (Tool icon) on the top right corner.
    1. Click 'Add' button.
    1. Enter `go-whistle` in the 'Enviroment Name' textbox (Can be any name of your option).
    1. Go to the table below, start adding variable as below:
        | VARIABLE | INITIAL VALUE | CURRENT VALUE |
        | ---- | ---- | ---- |
        | url | http://localhost:8888/counter/v1/statistics | http://localhost:8888/counter/v1/statistics |
    1. *You may adjust the 'INITIAL VALUE' and 'CURRENT VALUE' when an URL change.*
    1. *You may also introduce multiple enviroments for different environments testing.*

### Run the application
1. Go to '$GOPATH/src/go-whistle' folder.
1. Run the application with terminal.
    `go run main.go`
1. Open Postman.
1. Select 'go-whistle' environment
    1. Go to top right corner.
    1. On the left of the 'Eye' icon, click at the dropdown.
    1. Select 'go-whistle' environment.
1. Go to the 'Collection' tab on the left panel.
1. Expand the 'go-whistle' collection.
1. Click 'Send' button next to endpoint to call API.
    | API Name | Description |
    | ---- | ---- |
    | ping | Quick health check to see the connection to golang application is established. |
    | get-article | Adjust the article Id at the endpoint (Current ID set to 1. Article ID 1 to 4 are preset in database table setup above.) |
    | track-article | Adjust the article Id at the request JSON body. |
1. *If there is error occurs, you may do simple troubleshoot.*
    1. Check is the connection between Postman and go-whistle application by calling '/ping' API, expected result is 'Pong statistics v1'.
    1. If '503-Service not found' error,:
        - Check is the application is still running
        - Adjust the url with the steps in [Setup Postman - Create 'go-whistle' environment](#Setup-Postman) above.
    1. If error related to database connection failed,:
        - Check is Postgres server is running.
        - Adjust connection string based on steps in [Setup Database - Update connection string in golang application](#Setup-database) above.
