-- Database: go-whistle
DROP DATABASE IF EXISTS "go-whistle";
CREATE DATABASE "go-whistle"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;


-- SCHEMA: statistics
-- DROP SCHEMA statistics ;
CREATE SCHEMA statistics
    AUTHORIZATION postgres;
