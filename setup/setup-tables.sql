
DROP TABLE statistics.article_view_history;
DROP TABLE statistics.article;

-- Table: statistics.article
-- DROP TABLE statistics.article;
CREATE TABLE statistics.article
(
    id BIGSERIAL,
    name character varying(2048) COLLATE pg_catalog."default" NOT NULL,
    total_view_count integer NOT NULL,
    CONSTRAINT article_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;
ALTER TABLE statistics.article
    OWNER to postgres;


-- Table: statistics.article_view_history
-- DROP TABLE statistics.article_view_history;
CREATE TABLE statistics.article_view_history
(
    id BIGSERIAL,
    article_id bigint NOT NULL,
    read_ts timestamp(3) without time zone NOT NULL,
    CONSTRAINT "article_view_history_pkey" PRIMARY KEY (id),
    CONSTRAINT "article_id_fkey" FOREIGN KEY (article_id)
        REFERENCES statistics.article (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
)

TABLESPACE pg_default;

ALTER TABLE statistics.article_view_history
    OWNER to postgres;


INSERT INTO statistics.article(name, total_view_count) VALUES('abc', 0);
INSERT INTO statistics.article(name, total_view_count) VALUES('hello world in golang', 0);
INSERT INTO statistics.article(name, total_view_count) VALUES('Covid-19: The weapon from lab', 0);
INSERT INTO statistics.article(name, total_view_count) VALUES('Beautiful day comes with hope', 0);

-- TEST DATA
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-13 22:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-14 22:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-15 02:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-15 22:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-16 02:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-16 12:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, '2020-03-16 22:55:16.046');
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, (NOW() - interval '1 hours 30 minutes'));
INSERT INTO statistics.article_view_history(article_id, read_ts) VALUES(1, NOW());
