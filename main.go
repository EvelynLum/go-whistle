package main

import (
	"fmt"
	stat1 "go-whistle-api/statistics/v1/handler"
	stat2 "go-whistle-api/statistics/v2/handler"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := Router()

	fmt.Println("Starting server on the port 8888...")
	log.Fatal(http.ListenAndServe(":8888", r))
}

func Router() *mux.Router {
	r := mux.NewRouter()

	sr1 := r.PathPrefix("/counter/v1/statistics").Subrouter()
	sr1.HandleFunc("/ping", stat1.Ping).Methods("GET")
	sr1.HandleFunc("/track_article", stat1.TrackArticle).Methods("POST")
	sr1.HandleFunc("/article_id/{id}", stat1.GetArticleViewCount).Methods("GET")

	sr2 := r.PathPrefix("/counter/v2/statistics").Subrouter()
	sr2.HandleFunc("/ping", stat2.Ping).Methods("GET")

	return r
}
